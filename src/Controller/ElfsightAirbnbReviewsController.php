<?php

namespace Drupal\elfsight_airbnb_reviews\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightAirbnbReviewsController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/airbnb-reviews/?utm_source=portals&utm_medium=drupal&utm_campaign=airbnb-reviews&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
